import boto3
import os
from cocore.config import Config
from cocore.Logger import Logger
import pyodbc
import  sqlalchemy
#from codb.mssql_tools import MSSQLInteraction
from sqlalchemy import create_engine
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

LOG = Logger()
CONF = Config()

def connect_to_sql(env, database='Comp_Analysis'):
    if env == 'prod':
        print("Connecting to sql Production")
        server = CONF['BigMoneySQL']['server']
        username = CONF['BigMoneySQL']['user']
        password = CONF['BigMoneySQL']['password']
        driver = 'ODBC Driver 17 for SQL Server'

        conn = pyodbc.connect(
            'DRIVER={' + driver + '};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
        print("Connected to sql Production")
    else:
        server = CONF['BigMoneySQL-test']['server']
        username = CONF['BigMoneySQL-test']['user']
        password = CONF['BigMoneySQL-test']['password']
        driver = 'ODBC Driver 17 for SQL Server'

        conn = pyodbc.connect(
            'DRIVER={' + driver + '};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    return conn

def connect_to_engine(env, database = 'Comp_Analysis'):
    if env == 'prod':
        print("Creating Engine")
        server = CONF['BigMoneySQL']['server']
        username = CONF['BigMoneySQL']['user']
        password = CONF['BigMoneySQL']['password']
        driver = 'ODBC Driver 17 for SQL Server'
        try:
            eng = create_engine(
                r"mssql+pyodbc://{USER}:{PASSWORD}@{ENV}:1433/{DATABASE}?charset=utf8&driver={DRIVER}".format(
                    USER=username,
                    PASSWORD=password,
                    ENV=server,
                    DATABASE=database,
                    DRIVER=driver))
            print("Engine Created")
        except Exception as e:
            print(e)
    else:
        print("Creating Engine non prod")
        server = CONF['BigMoneySQL-test']['server']
        username = CONF['BigMoneySQL-test']['user']
        password = CONF['BigMoneySQL-test']['password']
        driver = 'ODBC Driver 17 for SQL Server'

        try:
            eng = create_engine(
                r"mssql+pyodbc://{USER}:{PASSWORD}@{ENV}:1433/{DATABASE}?charset=utf8&driver={DRIVER}".format(
                    USER=username,
                    PASSWORD=password,
                    ENV=server,
                    DATABASE=database,
                    DRIVER=driver))
        except Exception as e:
            print(e)

    return eng

def connect_to_s3(environment,filename,destination):
    if environment == 'prod':
        print("Connecting to s3 Production")
        AWS_ACCESS_KEY = CONF['lift']['aws_access_key']
        AWS_SECRET_KEY = CONF['lift']['aws_secret_key']
        bucket_name = CONF['lift']['s3bucket']
        s3 = boto3.resource(
            's3',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_KEY
            )
        try:
            s3.Bucket(bucket_name).download_file(filename, destination)
            print("File Downloaded from S3")
        except Exception as e:
            print(e)
    else:
        print("Connecting to s3 NonProduction")
        AWS_ACCESS_KEY = CONF['lift']['aws_access_key']
        AWS_SECRET_KEY = CONF['lift']['aws_secret_key']
        bucket_name = CONF['lift']['s3bucket']
        s3 = boto3.resource(
            's3',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_KEY
        )
        try:
            s3.Bucket(bucket_name).download_file(filename, destination)
        except Exception as e:
            print(e)

def upload_to_s3(environment,path,filename,destination, bucket= 'upload_bucket'):
    if environment == 'prod':
        print("Uploading to s3 Production")
        AWS_ACCESS_KEY = CONF['lift']['aws_access_key']
        AWS_SECRET_KEY = CONF['lift']['aws_secret_key']
        bucket_name = CONF['lift'][bucket]
        s3 = boto3.resource(
            's3',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_KEY
            )
        try:
            # s3.upload_file(filename, bucket_name)
            s3.Bucket(bucket_name).upload_file(path+filename, '{}/{}'.format(destination, filename))
            print("File Uploaded to S3")
        except Exception as e:
            print(e)
    else:
        print("Uploading to s3 NonProduction")
        AWS_ACCESS_KEY = CONF['lift']['aws_access_key']
        AWS_SECRET_KEY = CONF['lift']['aws_secret_key']
        bucket_name = CONF['lift']['s3bucket']
        s3 = boto3.resource(
            's3',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_KEY
        )
        try:
            s3.Bucket(bucket_name).upload_file(path + filename, '{}/{}'.format(destination, filename))
        except Exception as e:
            print(e)

def cleanup_file(filename):
    """Remove files from directory"""
    try:
        if filename:
            print("Removing file: {}".format(filename))
            os.remove(filename)
    except Exception as ex:
        print(ex.message)



def send_email_with_attachment(path, from_addr, body, Subject, To_addr, filename, cc):

        aws_id = CONF['lift']['aws_access_key']
        aws_key = CONF['lift']['aws_secret_key']
        region = 'us-east-1'

        msg = MIMEMultipart()

        msg['Subject'] = Subject
        msg['From'] = from_addr
        msg['To'] = To_addr
        msg['Cc'] = cc

        # print(msg.as_string())

        # the message body
        part = MIMEText(body, 'html', 'utf-8')

        msg.attach(part)
        # the attachment
        part = MIMEApplication(open(path, 'rb').read())
        part.add_header('Content-Disposition', 'attachment', filename=filename)
        msg.attach(part)

        connection = boto3.client(
            'ses',
            region_name=region,
            aws_access_key_id=aws_id,
            aws_secret_access_key=aws_key
        )

        send_attachment_args = {
            'Source': from_addr,
            'RawMessage': {
                'Data': msg.as_string()
            }
        }
        connection.send_raw_email(**send_attachment_args)
        LOG.l("Email with attachment has been sent")
