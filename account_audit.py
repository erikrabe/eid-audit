import argparse
import sys
import pandas as pd
import pyodbc
from openpyxl import load_workbook
import time
from cocore.Logger import Logger
from cocore.config import Config
from datetime import datetime
from datetime import timedelta
from helper import connect_to_sql, send_email_with_attachment
from sqlalchemy import event

log = Logger()
CONF = Config()

def parse_args(args):
    parser = argparse.ArgumentParser(
        description='Account audit\n'
                    'Contact: tanay.dalmia@equinox.com')
    parser.add_argument('-env',
                        help="Specify the environment in which to run this script. Prod, Staging or QA.",
                        choices=['prod', 'stag', 'qa'], default='qa')
    user_args = parser.parse_args(args)
    return user_args

def retrieve_account_audit(conn):
    query = "select a.LogonName, a.personid, awe.EecEEID, awe.Employee_Number, awe.Timeclock_Code, a.FriendlyName, a.EmployeeID, awe.Most_Recent_Term_DateTime_Processed,awe.Job_Title from EmpowerID2016.dbo.account a left join chronos2.stg.ultipro_rpt_awesome as awe on (awe.Timeclock_Code = '' and a.EmployeeID=awe.Employee_Number ) or (awe.Timeclock_Code <> '' and a.EmployeeID = awe.Timeclock_Code) where a.AccountStoreID = 305 and awe.Employment_Status_Code = 't' and a.Deleted = 0 and a.[Disabled] = 0"
    df = pd.read_sql_query(query, conn)
    df.to_csv('audit-reports/account_audit.csv')

def send_email():
    from_addr = "businesssystems@equinox.com"
    body = """Dear Team, <br/> 
                    Please see attached audit of Active AD accounts belonging to deactivated UKG records.
                     <br/> <br/> 

                     Regards <br/> 
                     Business System"""
    Subject = 'Monthly Provisioning AD Account Audit'
    To_addr = 'erik.rabe@equinox.com'
    cc_addr = 'sean.orourke@equinox.com,david.chu@equinox.com'
    filename = 'account_audit.csv'

    send_email_with_attachment('audit-reports/' + filename, from_addr, body, Subject, To_addr, filename, cc_addr)


if __name__ == '__main__':
    args = parse_args(sys.argv[1:])
    conn = connect_to_sql(args.env)
    retrieve_account_audit(conn)
    send_email()
    conn.close()




