# EID Audit
This project helps in Auditing EMPOWER

## Installation

For local development, using a virtual environment is recommended. Install dependencies with:

    pip install -r requirements.txt

Add a file `etl.cfg`.

## Execute

python3 account_audit.py -env prod
